<?php
/**
* @package Helix3 Framework
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2015 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/

defined('_JEXEC') or die;
JHtml::_('jquery.framework', true, null, true);
JHtml::_('bootstrap.framework');
JHtml::_('formbehavior.chosen', 'select');
$doc = JFactory::getDocument();
$app = JFactory::getApplication();

//Load Helix
$helix_path = JPATH_PLUGINS . '/system/helixultimate/core/helixultimate.php';

if (file_exists($helix_path)) {
    require_once($helix_path);
    $theme = new helixUltimate;
} else {
    die('Install and activate <a target="_blank" href="https://www.joomshaper.com/helix">Helix Ultimate Framework</a>.');
}

$webfonts = array();
 if ($this->params->get('enable_custom_font2') && $this->params->get('custom_font_selectors2'))
{
    $webfonts[$this->params->get('custom_font_selectors2')] = $this->params->get('custom_font2');
}
if ($this->params->get('enable_btn_font'))
{
 $webfonts['.btn , .sppb-btn'] = $this->params->get('btn_font');
}
if ($this->params->get('enable_form_font'))
{
 $webfonts['.sppb-form-control,.controls .input-prepend input, .sppb-form-group .sppb-form-control,select,textarea,input[type="text"],input[type="password"],input[type="datetime"],input[type="datetime-local"],input[type="date"],input[type="dates"],input[type="month"],input[type="time"],input[type="times"],input[type="week"],input[type="number"],input[type="email"],input[type="url"],input[type="search"],input[type="tel"],input[type="color"],.uneditable-input,div.chzn-container.chzn-container-single .chzn-single > span,div.chzn-container.chzn-container-single .chzn-results li'] = $this->params->get('form_font');
}

$theme->addGoogleFont($webfonts);
//Coming Soon
if ($this->params->get('comingsoon'))
{
  header("Location: " . $this->baseUrl . "?tmpl=comingsoon");
}

$custom_style = $this->params->get('custom_style');
$preset = $this->params->get('preset');
if($custom_style || !$preset)
{
$scssVars = array(
    'preset' => 'default',
    'global_color' => $this->params->get('global_color'),
    'global_color2' => $this->params->get('global_color2'),
    'global_color3' => $this->params->get('global_color2'),
    'text_colors' => $this->params->get('text_colors'),
    'bg_color' => $this->params->get('bg_color'),
    'header_bg_color' => $this->params->get('header_bg_color'),
    'logo_text_color' => $this->params->get('logo_text_color'),
    'menu_text_color' => $this->params->get('menu_text_color'),
    'menu_text_hover_color' => $this->params->get('menu_text_hover_color'),
    'menu_text_active_color' => $this->params->get('menu_text_active_color'),
    'menu_dropdown_bg_color' => $this->params->get('menu_dropdown_bg_color'),
    'menu_dropdown_text_color' => $this->params->get('menu_dropdown_text_color'),
    'menu_dropdown_text_hover_color' => $this->params->get('menu_dropdown_text_hover_color'),
    'menu_dropdown_text_active_color' => $this->params->get('menu_dropdown_text_active_color'),
    'footer_bg_color' => $this->params->get('footer_bg_color'),
    'footer_text_color' => $this->params->get('footer_text_color'),
    'footer_link_color' => $this->params->get('footer_link_color'),
    'footer_link_hover_color' => $this->params->get('footer_link_hover_color'),
    'topbar_bg_color' => $this->params->get('topbar_bg_color'),
    'topbar_text_color' => $this->params->get('topbar_text_color')
);
}
else
{
    $scssVars = (array) json_decode($this->params->get('preset'));
}
$scssVars['header_height'] = $this->params->get('header_height', '60px');
$scssVars['offcanvas_width'] = $this->params->get('offcanvas_width', '300') . 'px';

$scssVars['header_height'] = $this->params->get('header_height', '60px');
$scssVars['offcanvas_width'] = $this->params->get('offcanvas_width', '300') . 'px';


//Body Background Image
if ($bg_image = $this->params->get('body_bg_image'))
{
    $body_style = 'background-image: url(' . JURI::base(true) . '/' . $bg_image . ');';
    $body_style .= 'background-repeat: ' . $this->params->get('body_bg_repeat') . ';';
    $body_style .= 'background-size: ' . $this->params->get('body_bg_size') . ';';
    $body_style .= 'background-attachment: ' . $this->params->get('body_bg_attachment') . ';';
    $body_style .= 'background-position: ' . $this->params->get('body_bg_position') . ';';
    $body_style = 'body.site {' . $body_style . '}';
    $doc->addStyledeclaration($body_style);
}

//Custom CSS
if ($custom_css = $this->params->get('custom_css'))
{
    $doc->addStyledeclaration($custom_css);
}

//Custom JS
if ($custom_js = $this->params->get('custom_js'))
{
    $doc->addScriptdeclaration($custom_js);
}

$comingsoon_title = $this->params->get('comingsoon_title');
if( $comingsoon_title ) {
	$doc->setTitle( $comingsoon_title . ' | ' . $app->get('sitename') );
}

$comingsoon_date = explode('-', $this->params->get("comingsoon_date"));
 //print_r($this->params->get('comingsoon'));
//Load jQuery
JHtml::_('jquery.framework');

?>

<html class="sp-comingsoon" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="canonical" href="<?php echo JUri::current(); ?>">
        <?php

        $theme->head();
        
        $theme->add_css('font-awesome.min.css,linearicons.css,owl.carousel.min.css,owl.transitions.css,owl.theme.default.min.css');
        $theme->add_js('jquery.sticky.js, main.js,owl.carousel.min.js');

        $theme->add_scss('master', $scssVars, 'template');

        if($this->direction == 'rtl')
        {
            $theme->add_scss('rtl', $scssVars, 'rtl');
        }
        $theme->add_scss('presets', $scssVars, 'presets/' . $scssVars['preset']);

        $theme->add_css('custom');

        //Before Head
        if ($before_head = $this->params->get('before_head'))
        {
            echo $before_head . "\n";
        }
        ?>
    </head>
    <body class="<?php echo $theme->bodyClass(); ?>">
    <?php if($this->params->get('preloader')) : ?>
        <div class="sp-preloader"><div></div></div>
    <?php endif; ?>

    <div class="body-wrapper">
        <div class="body-innerwrapper">
            <div class="comingsoon-page-logo">
                <div class="container">
                    <div class="text-center">
                        <div id="sp-comingsoon">
                            <?php
                             if($comingsoon_logo = $this->params->get('comingsoon_logo')){ ?>
                                <img class="comingsoon-logo" alt="logo" src="<?php echo JURI::root() . '/' .  $comingsoon_logo; ?>" />
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>                
           <?php if($this->countModules('comingsoon')) { ?>
				<div class="sp-position-comingsoon">
					<jdoc:include type="modules" name="comingsoon" style="sp_xhtml" />
				</div>
			<?php } ?>
        </div>
        <div id="copyright">
        <div class="text-center">
        <?php 
            echo '<span class="sp-copyright">' . str_ireplace('{year}',date('Y'), str_ireplace('joomshaper', '', $this->params->get('copyright'))) . '</span>';
        ?>
        </div></div>
    </div>
    </body>
</html>