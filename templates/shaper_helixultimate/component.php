<?php
/**
 * @package Helix Ultimate Framework
 * @author JoomShaper https://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/

defined ('_JEXEC') or die();

use Joomla\CMS\Factory;
use Joomla\CMS\Uri\Uri;

$theme_url = URI::base(true) . '/templates/'. $this->template;
$app = Factory::getApplication();
$option = $app->input->get('option', '', 'STRING');

$body_class = htmlspecialchars(str_replace('_', '-', $option));
$body_class .= ' view-' . htmlspecialchars($app->input->get('view', '', 'STRING'));
$body_class .= ' layout-' . htmlspecialchars($app->input->get('layout', 'default', 'STRING'));
$body_class .= ' task-' . htmlspecialchars($app->input->get('task', 'none', 'STRING'));

$helix_path = JPATH_PLUGINS . '/system/helixultimate/core/helixultimate.php';

if (file_exists($helix_path)) {
    require_once($helix_path);
    $theme = new helixUltimate;
} else {
    die('Install and activate <a target="_blank" href="https://www.joomshaper.com/helix">Helix Ultimate Framework</a>.');
}
$custom_style = $this->params->get('custom_style');
$preset = $this->params->get('preset');
if($custom_style || !$preset)
{
$scssVars = array(
    'preset' => 'default',
    'global_color' => $this->params->get('global_color'),
    'global_color2' => $this->params->get('global_color2'),
    'global_color3' => $this->params->get('global_color2'),
    'text_colors' => $this->params->get('text_colors'),
    'bg_color' => $this->params->get('bg_color'),
    'header_bg_color' => $this->params->get('header_bg_color'),
    'logo_text_color' => $this->params->get('logo_text_color'),
    'menu_text_color' => $this->params->get('menu_text_color'),
    'menu_text_hover_color' => $this->params->get('menu_text_hover_color'),
    'menu_text_active_color' => $this->params->get('menu_text_active_color'),
    'menu_dropdown_bg_color' => $this->params->get('menu_dropdown_bg_color'),
    'menu_dropdown_text_color' => $this->params->get('menu_dropdown_text_color'),
    'menu_dropdown_text_hover_color' => $this->params->get('menu_dropdown_text_hover_color'),
    'menu_dropdown_text_active_color' => $this->params->get('menu_dropdown_text_active_color'),
    'footer_bg_color' => $this->params->get('footer_bg_color'),
    'footer_text_color' => $this->params->get('footer_text_color'),
    'footer_link_color' => $this->params->get('footer_link_color'),
    'footer_link_hover_color' => $this->params->get('footer_link_hover_color'),
    'topbar_bg_color' => $this->params->get('topbar_bg_color'),
    'topbar_text_color' => $this->params->get('topbar_text_color')
);
}
else
{
    $scssVars = (array) json_decode($this->params->get('preset'));
}
$scssVars['header_height'] = $this->params->get('header_height', '60px');
$scssVars['offcanvas_width'] = $this->params->get('offcanvas_width', '300') . 'px';

$scssVars['header_height'] = $this->params->get('header_height', '60px');
$scssVars['offcanvas_width'] = $this->params->get('offcanvas_width', '300') . 'px';

?>
<!doctype html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if ($favicon = $this->params->get('favicon')) : ?>
    <link rel="icon" href="<?php echo URI::base(true) . '/' . $favicon; ?>" />
    <?php else: ?>
    <link rel="icon" href="<?php echo $theme_url .'/images/favicon.ico'; ?>" />
    <?php endif; ?>

    <jdoc:include type="head" />

    <?php
    //print_r($theme);
       //$theme->head();
        $theme->add_scss('presets', $scssVars, 'presets/' . $scssVars['preset']);

     if($option != 'com_sppagebuilder') : ?>
        <?php if(file_exists( \JPATH_THEMES . '/' . $this->template . '/css/bootstrap.min.css' )) : ?>
        <link href="<?php echo $theme_url . '/css/bootstrap.min.css'; ?>" rel="stylesheet">
        <?php else: ?>
        <link href="<?php echo URI::base(true) . '/plugins/system/helixultimate/css/bootstrap.min.css'; ?>" rel="stylesheet">
        <?php endif; ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
         <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/linearicons.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/font-awesome.min.css" type="text/css" />

    <?php endif; ?>
    
  </head>

  <body class="contentpane <?php echo $body_class; ?>">
    <jdoc:include type="component" />
  </body>
</html>
