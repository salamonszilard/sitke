<?php
/**
 * @version    2.8.x
 * @package    K2
 * @author     JoomlaWorks http://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2017 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="sppb-addon-articles k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

	<?php if($params->get('itemPreText')): ?>
	<p class="modulePretext"><?php echo $params->get('itemPreText'); ?></p>
	<?php endif; ?>

	<?php if(count($items)): ?>
	<div class="blog-list blog-custom">
  <ul>
    <?php foreach ($items as $key=>$item):	?>
    <li class="<?php echo ($key%2) ? "odd" : "even"; if(count($items)==$key+1) echo ' lastItem'; ?> sppb-col-sm-12">
    	<div class="addon-article">
      <!-- Plugins: BeforeDisplay -->
      <?php echo $item->event->BeforeDisplay; ?>

      <!-- K2 Plugins: K2BeforeDisplay -->
      <?php echo $item->event->K2BeforeDisplay; ?>
		<!-- K2 Fleft -->
		<div class="image-box">
	      <?php if($params->get('itemImage')): ?>
		      <?php if($params->get('itemImage') && isset($item->image)): ?>
		      <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
		      	<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
		      </a>
		      <?php endif; ?>
	      <?php endif; ?>
		</div>
 		<!-- K2 Fright -->
 		<div class="blog-box">
      <?php if($params->get('itemTitle')): ?>
      <div class="name-blog"><a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></div>
      <?php endif; ?>
	<div class="sppb-article-meta">

      <?php if($params->get('itemCategory')): ?>
		 <span class="sppb-meta-category"><i class="linearicons-folder"></i><?php echo $item->categoryname; ?></span>
      <?php endif; ?>

      <?php if($params->get('itemDateCreated')): ?>
      <span class="sppb-meta-date" itemprop="dateCreated"><i class="linearicons-calendar-full"></i><?php echo JHTML::_('date', $item->created, JText::_('DATE_FORMAT_LC3')); ?></span>
      <?php endif; ?>

		<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>
			<span class="komento"><i class="linearicons-bubble"></i><?php echo $item->numOfComments; ?></span>
		<?php endif; ?>

		<?php if($params->get('itemHits')): ?>
		<span class="hits"><i class="linearicons-heart"></i><?php echo $item->hits; ?></span>
		<?php endif; ?>
		</div>
		</div>
		<?php if($params->get('itemReadMore') && $item->fulltext): ?>
		<a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
			<?php echo JText::_('K2_READ_MORE'); ?>
		</a>
		<?php endif; ?>
		</div>
    </li>
    <?php endforeach; ?>
    <li class="clearList"></li>
  </ul>
  </div>
  <?php endif; ?>

	<?php if($params->get('itemCustomLink')): ?>
	<a class="moduleCustomLink" href="<?php echo $itemCustomLinkURL; ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>"><?php echo $itemCustomLinkTitle; ?></a>
	<?php endif; ?>

	<?php if($params->get('feed')): ?>
	<div class="k2FeedIcon">
		<a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&format=feed&moduleID='.$module->id); ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
			<i class="icon-feed"></i>
			<span><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

</div>
