jQuery.fn.endlessRiver = function (settings) {
    settings = jQuery.extend({
        speed: 100,
        pause: true,
        buttons: false
    }, settings);
    return this.each(function(){
        var j = jQuery;
        var $line = j(this);
        var id = "ER_"+ new Date().getTime();
        $line.wrap("<div id=\""+id+"\"></div>");
        $line.css({
            margin: "0 !important",
            padding: "0 !important"
        });
        var currentSpazio,currentTempo;
        var run = true;
        var initialOffset = $line.offset().left;
        var lineWidth = 1;
        $line.children("li.tick-clones").remove();
        //elimina cloni se ci sono - Serve in caso io aggiorni dinamicamente il contenuto
        $line.addClass("newsticker");
        var $mask = $line.wrap("<div class='mask'></div>");
        var $tickercontainer = $line.parent().wrap("<div class='tickercontainer'></div>");
        var elements = $line.children("li");
        var fill = function(){
            lineWidth = 1;
            $line.append(elements.clone(true).addClass("tick-clones"));
            $line.children("li").each(function (i) {
                lineWidth += j(this, i).outerWidth(true);
                //outherWidth con argomento true ritorna larghezza compresi margini
            });
            
        }
        var l = $tickercontainer.outerWidth(true);
        while(lineWidth<l) fill();
        $line.width(lineWidth);
        $line.height($line.parent().height());
        function scrollnews(spazio, tempo) {
            $line.animate({left: '-=' + spazio}, tempo, "linear", function () {
                $line.children("li:first").appendTo($line);
                $line.css("left", 0);
                currentSpazio = $line.children("li:first").outerWidth(true);
                currentTempo = tempo / spazio * currentSpazio;
                if(run)
                    scrollnews(currentSpazio, currentTempo);
            });
        }
        //BOOT
        currentSpazio = $line.children("li:first").outerWidth(true);
        currentTempo = currentSpazio / settings.speed * 1000;
        //x 1000 perchè tempo è in millisecondi
        scrollnews(currentSpazio, currentTempo);
        function setHover(){
            $line.hover(pause,resume);
        }

        function pause(){
            run = false;
            $line.stop();
        }

        function resume() {
            run = true;
            var offset = $line.offset().left;
            var residualSpace = offset + $line.children("li:first").outerWidth(true) - initialOffset;
            var residualTime = currentTempo / currentSpazio * residualSpace;
            scrollnews(residualSpace, residualTime);
        }
        if(settings.pause) setHover();
        
        if(settings.buttons){

            var $buttons = j('<ul class="er-controls">'+
            '<li class="prev glyphicon glyphicon-chevron-left"></li>'+
            '<li class="pause glyphicon glyphicon-pause"></li>'+
            '<li class="next glyphicon glyphicon-chevron-right"></li>'+
            '</ul>');
            $buttons.insertAfter($tickercontainer);
            //DELEGATE IS BETTER!
            j("body").on("click", "#"+id+" .er-controls > .pause", function(){
                if(!run) return false;
                j(this).toggleClass("pause glyphicon-pause play glyphicon-play");
                $line.unbind('mouseenter mouseleave');
                run = false;
            });

            j("body").on("click", "#"+id+" .er-controls > .play", function(){
                if(run) return false;
                j(this).toggleClass("pause glyphicon-pause play glyphicon-play");
                run = true;
                setHover();
                var offset = $line.offset().left;
                var residualSpace = offset + $line.children("li:first").outerWidth(true) - initialOffset;
                var residualTime = currentTempo / currentSpazio * residualSpace;
                scrollnews(residualSpace, residualTime);
            });

            var moving = false;
            
            j("body").on("click", "#"+id+" .er-controls > .next", function(){
                if(run){
                    j("#"+id+" .er-controls > .pause").toggleClass("pause glyphicon-pause play glyphicon-play");
                    run = false;
                    return;
                }
                if(moving) return false;
                var spazio = $line.children("li:first").outerWidth(true);
                var tempo = spazio / settings.speed * 1000;
                moving = true;
                $line.stop(true,true).animate({left: '-=' + spazio}, tempo, "linear", function () {
                    $line.children("li:first").appendTo($line);
                    $line.css("left", 0);
                    moving = false;
                });

            });

            j("body").on("click", "#"+id+" .er-controls > .prev", function(){
                if(run){
                    j("#"+id+" .er-controls > .pause").toggleClass("pause glyphicon-pause play glyphicon-play");
                    run = false;
                    return;
                } 
                if(moving) return false;
                var spazio = $line.children("li:last").outerWidth(true);
                $line.css("left", "-"+spazio+"px");
                $line.children("li:last").prependTo($line);
                var tempo = spazio / settings.speed * 1000;
                moving = true;
                $line.stop(true,true).animate({left: '+=' + spazio}, tempo, "linear", function(){
                    moving = false;
                });
                
            });         
        }
            
    });
};
/*!
 * SlickNav Responsive Mobile Menu v1.0.10
 * (c) 2016 Josh Cope
 * licensed under MIT
 */
!function(e,t,n){function a(t,n){this.element=t,this.settings=e.extend({},i,n),this.settings.duplicate||n.hasOwnProperty("removeIds")||(this.settings.removeIds=!1),this._defaults=i,this._name=s,this.init()}var i={label:"MENU",duplicate:!0,duration:200,easingOpen:"swing",easingClose:"swing",closedSymbol:"&#9658;",openedSymbol:"&#9660;",prependTo:"body",appendTo:"",parentTag:"a",closeOnClick:!1,allowParentLinks:!1,nestedParentLinks:!0,showChildren:!1,removeIds:!0,removeClasses:!1,removeStyles:!1,brand:"",animations:"jquery",init:function(){},beforeOpen:function(){},beforeClose:function(){},afterOpen:function(){},afterClose:function(){}},s="slicknav",o="slicknav",l={DOWN:40,ENTER:13,ESCAPE:27,LEFT:37,RIGHT:39,SPACE:32,TAB:9,UP:38};a.prototype.init=function(){var n,a,i=this,s=e(this.element),r=this.settings;if(r.duplicate?i.mobileNav=s.clone():i.mobileNav=s,r.removeIds&&(i.mobileNav.removeAttr("id"),i.mobileNav.find("*").each(function(t,n){e(n).removeAttr("id")})),r.removeClasses&&(i.mobileNav.removeAttr("class"),i.mobileNav.find("*").each(function(t,n){e(n).removeAttr("class")})),r.removeStyles&&(i.mobileNav.removeAttr("style"),i.mobileNav.find("*").each(function(t,n){e(n).removeAttr("style")})),n=o+"_icon",""===r.label&&(n+=" "+o+"_no-text"),"a"==r.parentTag&&(r.parentTag='a href="#"'),i.mobileNav.attr("class",o+"_nav"),a=e('<div class="'+o+'_menu"></div>'),""!==r.brand){var c=e('<div class="'+o+'_brand">'+r.brand+"</div>");e(a).append(c)}i.btn=e(["<"+r.parentTag+' aria-haspopup="true" role="button" tabindex="0" class="'+o+"_btn "+o+'_collapsed">','<span class="'+o+'_menutxt">'+r.label+"</span>",'<span class="'+n+'">','<span class="'+o+'_icon-bar"></span>','<span class="'+o+'_icon-bar"></span>','<span class="'+o+'_icon-bar"></span>',"</span>","</"+r.parentTag+">"].join("")),e(a).append(i.btn),""!==r.appendTo?e(r.appendTo).append(a):e(r.prependTo).prepend(a),a.append(i.mobileNav);var p=i.mobileNav.find("li");e(p).each(function(){var t=e(this),n={};if(n.children=t.children("ul").attr("role","menu"),t.data("menu",n),n.children.length>0){var a=t.contents(),s=!1,l=[];e(a).each(function(){return e(this).is("ul")?!1:(l.push(this),void(e(this).is("a")&&(s=!0)))});var c=e('');if(r.allowParentLinks&&!r.nestedParentLinks&&s)e(l).wrapAll('<span class="'+o+"_parent-link "+o+'_row"/>').parent();else{var p=e(l).wrapAll(c).parent();p.addClass(o+"_row")}r.showChildren?t.addClass(o+"_open"):t.addClass(o+"_collapsed"),t.addClass(o+"_parent");var d=e(''+(r.showChildren?r.openedSymbol:r.closedSymbol)+"</span>");r.allowParentLinks&&!r.nestedParentLinks&&s&&(d=d.wrap(c).parent()),e(l).last().after(d)}else 0===t.children().length&&t.addClass(o+"_txtnode");t.children("a").attr("role","menuitem").click(function(t){r.closeOnClick&&!e(t.target).parent().closest("li").hasClass(o+"_parent")&&e(i.btn).click()}),r.closeOnClick&&r.allowParentLinks&&(t.children("a").children("a").click(function(t){e(i.btn).click()}),t.find("."+o+"_parent-link a:not(."+o+"_item)").click(function(t){e(i.btn).click()}))}),e(p).each(function(){var t=e(this).data("menu");r.showChildren||i._visibilityToggle(t.children,null,!1,null,!0)}),i._visibilityToggle(i.mobileNav,null,!1,"init",!0),i.mobileNav.attr("role","menu"),e(t).mousedown(function(){i._outlines(!1)}),e(t).keyup(function(){i._outlines(!0)}),e(i.btn).click(function(e){e.preventDefault(),i._menuToggle()}),i.mobileNav.on("click","."+o+"_item",function(t){t.preventDefault(),i._itemClick(e(this))}),e(i.btn).keydown(function(t){var n=t||event;switch(n.keyCode){case l.ENTER:case l.SPACE:case l.DOWN:t.preventDefault(),n.keyCode===l.DOWN&&e(i.btn).hasClass(o+"_open")||i._menuToggle(),e(i.btn).next().find('[role="menuitem"]').first().focus()}}),i.mobileNav.on("keydown","."+o+"_item",function(t){var n=t||event;switch(n.keyCode){case l.ENTER:t.preventDefault(),i._itemClick(e(t.target));break;case l.RIGHT:t.preventDefault(),e(t.target).parent().hasClass(o+"_collapsed")&&i._itemClick(e(t.target)),e(t.target).next().find('[role="menuitem"]').first().focus()}}),i.mobileNav.on("keydown",'[role="menuitem"]',function(t){var n=t||event;switch(n.keyCode){case l.DOWN:t.preventDefault();var a=e(t.target).parent().parent().children().children('[role="menuitem"]:visible'),s=a.index(t.target),r=s+1;a.length<=r&&(r=0);var c=a.eq(r);c.focus();break;case l.UP:t.preventDefault();var a=e(t.target).parent().parent().children().children('[role="menuitem"]:visible'),s=a.index(t.target),c=a.eq(s-1);c.focus();break;case l.LEFT:if(t.preventDefault(),e(t.target).parent().parent().parent().hasClass(o+"_open")){var p=e(t.target).parent().parent().prev();p.focus(),i._itemClick(p)}else e(t.target).parent().parent().hasClass(o+"_nav")&&(i._menuToggle(),e(i.btn).focus());break;case l.ESCAPE:t.preventDefault(),i._menuToggle(),e(i.btn).focus()}}),r.allowParentLinks&&r.nestedParentLinks&&e("."+o+"_item a").click(function(e){e.stopImmediatePropagation()})},a.prototype._menuToggle=function(e){var t=this,n=t.btn,a=t.mobileNav;n.hasClass(o+"_collapsed")?(n.removeClass(o+"_collapsed"),n.addClass(o+"_open")):(n.removeClass(o+"_open"),n.addClass(o+"_collapsed")),n.addClass(o+"_animating"),t._visibilityToggle(a,n.parent(),!0,n)},a.prototype._itemClick=function(e){var t=this,n=t.settings,a=e.data("menu");a||(a={},a.arrow=e.children('"."+o+"_arrow"'),a.ul=e.next("ul"),a.parent=e.parent(),a.parent.hasClass(o+"_parent-link")&&(a.parent=e.parent().parent(),a.ul=e.parent().next("ul")),e.data("menu",a)),a.parent.hasClass(o+"_collapsed")?(a.arrow.html(n.openedSymbol),a.parent.removeClass(o+"_collapsed"),a.parent.addClass(o+"_open"),a.parent.addClass(o+"_animating"),t._visibilityToggle(a.ul,a.parent,!0,e)):(a.arrow.html(n.closedSymbol),a.parent.addClass(o+"_collapsed"),a.parent.removeClass(o+"_open"),a.parent.addClass(o+"_animating"),t._visibilityToggle(a.ul,a.parent,!0,e))},a.prototype._visibilityToggle=function(t,n,a,i,s){function l(t,n){e(t).removeClass(o+"_animating"),e(n).removeClass(o+"_animating"),s||p.afterOpen(t)}function r(n,a){t.attr("aria-hidden","true"),d.attr("tabindex","-1"),c._setVisAttr(t,!0),t.hide(),e(n).removeClass(o+"_animating"),e(a).removeClass(o+"_animating"),s?"init"==n&&p.init():p.afterClose(n)}var c=this,p=c.settings,d=c._getActionItems(t),u=0;a&&(u=p.duration),t.hasClass(o+"_hidden")?(t.removeClass(o+"_hidden"),s||p.beforeOpen(i),"jquery"===p.animations?t.stop(!0,!0).slideDown(u,p.easingOpen,function(){l(i,n)}):"velocity"===p.animations&&t.velocity("finish").velocity("slideDown",{duration:u,easing:p.easingOpen,complete:function(){l(i,n)}}),t.attr("aria-hidden","false"),d.attr("tabindex","0"),c._setVisAttr(t,!1)):(t.addClass(o+"_hidden"),s||p.beforeClose(i),"jquery"===p.animations?t.stop(!0,!0).slideUp(u,this.settings.easingClose,function(){r(i,n)}):"velocity"===p.animations&&t.velocity("finish").velocity("slideUp",{duration:u,easing:p.easingClose,complete:function(){r(i,n)}}))},a.prototype._setVisAttr=function(t,n){var a=this,i=t.children("li").children("ul").not("."+o+"_hidden");n?i.each(function(){var t=e(this);t.attr("aria-hidden","true");var i=a._getActionItems(t);i.attr("tabindex","-1"),a._setVisAttr(t,n)}):i.each(function(){var t=e(this);t.attr("aria-hidden","false");var i=a._getActionItems(t);i.attr("tabindex","0"),a._setVisAttr(t,n)})},a.prototype._getActionItems=function(e){var t=e.data("menu");if(!t){t={};var n=e.children("li"),a=n.find("a");t.links=a.add(n.find("."+o+"_item")),e.data("menu",t)}return t.links},a.prototype._outlines=function(t){t?e("."+o+"_item, ."+o+"_btn").css("outline",""):e("."+o+"_item, ."+o+"_btn").css("outline","none")},a.prototype.toggle=function(){var e=this;e._menuToggle()},a.prototype.open=function(){var e=this;e.btn.hasClass(o+"_collapsed")&&e._menuToggle()},a.prototype.close=function(){var e=this;e.btn.hasClass(o+"_open")&&e._menuToggle()},e.fn[s]=function(t){var n=arguments;if(void 0===t||"object"==typeof t)return this.each(function(){e.data(this,"plugin_"+s)||e.data(this,"plugin_"+s,new a(this,t))});if("string"==typeof t&&"_"!==t[0]&&"init"!==t){var i;return this.each(function(){var o=e.data(this,"plugin_"+s);o instanceof a&&"function"==typeof o[t]&&(i=o[t].apply(o,Array.prototype.slice.call(n,1)))}),void 0!==i?i:this}}}(jQuery,document,window);
/**
 * @package Helix Ultimate Framework
 * @author JoomShaper https://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/

jQuery(function ($) {

    $("#breaking").endlessRiver({speed: 50});
    // if sticky header
    if ($("body.sticky-header").length > 0) {
        if ($('#sp-header').hasClass('style-1') || $('#sp-header').hasClass('style-2')) {
             var fixedSection = $('#sp-header');
            // sticky nav
            var headerHeight = fixedSection.innerHeight();
            var stickyNavTop = fixedSection.offset().top;
            fixedSection.addClass('');
            fixedSection.before('<div class="nav-placeholder"></div>');
            $('.nav-placeholder').height('inherit');
            //add class
            fixedSection.addClass('menu-fixed-out');
            var stickyNav = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > stickyNavTop) {
                    fixedSection.removeClass('menu-fixed-out').addClass('menu-fixed');
                    $('.nav-placeholder').height(headerHeight);
                } else {
                    if (fixedSection.hasClass('menu-fixed')) {
                        fixedSection.removeClass('menu-fixed').addClass('menu-fixed-out');
                        $('.nav-placeholder').height('inherit');
                    }
                }
            };
            $(document).ready(function() {
                if ($('.sp-megamenu-parent').length > 0) {
                    var stickMenu = true;
                    var docWidth= $('body').find('#sp-header').width();
                    //if (stickMenu && docWidth > 780) {
                        stickyNav();
                    //} 
                }
            }); 
            $(window).scroll(function () {
               stickyNav();
            });
        }
    }
    if ($("body.sticky-header").length > 0) {
        if ($('#sp-header').hasClass('style-6') || $('#sp-header').hasClass('style-5') || $('#sp-header').hasClass('style-4') || $('#sp-header').hasClass('style-3')) {
             var fixedSection = $('#sp-menu');
            // sticky nav
            var headerHeight = fixedSection.innerHeight();
            var stickyNavTop = fixedSection.offset().top;
            fixedSection.addClass('');
            fixedSection.before('<div class="nav-placeholder"></div>');
            $('.nav-placeholder').height('inherit');
            //add class
            fixedSection.addClass('menu-fixed-out');
            var stickyNav2 = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > stickyNavTop) {
                    fixedSection.removeClass('menu-fixed-out').addClass('menu-fixed');
                    $('.nav-placeholder').height(headerHeight);
                } else {
                    if (fixedSection.hasClass('menu-fixed')) {
                        fixedSection.removeClass('menu-fixed').addClass('menu-fixed-out');
                        $('.nav-placeholder').height('inherit');
                    }
                }
            };
            $(document).ready(function() {
                if ($('.sp-megamenu-parent').length > 0) {
                    var stickMenu = true;
                    var docWidth= $('body').find('#sp-menu').width();
                    //if (stickMenu && docWidth > 780) {
                       stickyNav2();
                   // } 
                }
            }); 
             $(window).scroll(function () {
               stickyNav2();
            });
        }
    }
    // go to top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.sp-scroll-up').fadeIn();
        } else {
            $('.sp-scroll-up').fadeOut(400);
        }
    });

    $('.sp-scroll-up').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    // Preloader
    $(window).on('load', function () {
        $('.sp-pre-loader').fadeOut(500, function() {
            $(this).remove();
        });
    });

    //mega menu
    $('.sp-megamenu-wrapper').parent().parent().css('position', 'static').parent().css('position', 'relative');
    $('.sp-menu-full').each(function () {
        $(this).parent().addClass('menu-justify');
    });

    // Offcanvs
    $('#offcanvas-toggler').on('click', function (event) {
        event.preventDefault();
        $('.offcanvas-init').addClass('offcanvas-active');
    });

    $('.close-offcanvas, .offcanvas-overlay').on('click', function (event) {
        event.preventDefault();
        $('.offcanvas-init').removeClass('offcanvas-active');
    });
    
    $(document).on('click', '.offcanvas-inner .menu-toggler', function(event){
        event.preventDefault();
        $(this).closest('.menu-parent').toggleClass('menu-parent-open').find('>.menu-child').slideToggle(400);
    });

    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Article Ajax voting
    $('.article-ratings .rating-star').on('click', function (event) {
        event.preventDefault();
        var $parent = $(this).closest('.article-ratings');

        var request = {
            'option': 'com_ajax',
            'template': template,
            'action': 'rating',
            'rating': $(this).data('number'),
            'article_id': $parent.data('id'),
            'format': 'json'
        };

        $.ajax({
            type: 'POST',
            data: request,
            beforeSend: function () {
                $parent.find('.fa-spinner').show();
            },
            success: function (response) {
                var data = $.parseJSON(response);
                $parent.find('.ratings-count').text(data.message);
                $parent.find('.fa-spinner').hide();

                if(data.status)
                {
                    $parent.find('.rating-symbol').html(data.ratings)
                }

                setTimeout(function(){
                    $parent.find('.ratings-count').text('(' + data.rating_count + ')')
                }, 3000);
            }
        });
    });

});
;(function ($) {
    $(document).ready(function($){
        $('.sppb-section .sppb-row-overlay').each(function() { 
            $(this).next('.sppb-row-container').addClass('z-index');
        });
     $('.sp-dropdown-mega .sp-mega-group li').each(function() {
       if($(this).find('.sp-dropdown-items').hasClass('sp-mega-group-child')) {
        $(this).addClass('uppercase');
       } 
    });
    $('.sp-megamenu-parent').slicknav({
        parentTag: 'a',
        closeOnClick:false,
        nestedParentLinks:false,
        showChildren:true,
        prependTo:'.offcanvas-inner',
        removeClasses : false ,
        init:function(){

            if($('.slicknav_nav .sp-menu-item').hasClass('sp-has-child')){
                //alert();
                //$('.slicknav_nav .sp-menu-item.sp-has-child >a').after('<span class="fa icon"/>');
                //$('.slicknav_nav .sp-mega-group .item-header >a').after('<span class="fa icon"/>'); 
            }
            $('.slicknav_nav .sp-menu-item .sp-dropdown').css('display','none');


            $('.sp-dropdown-mega .sp-mega-group li').each(function() {
               if($(this).find('.sp-dropdown-items').hasClass('sp-mega-group-child')) {
                    //$(this).addClass('uppercase');
                    $(this).find('> a').after('<span class="fa icon"/>'); 
               } 
            });
            if($('.slicknav_nav .sp-menu-item').hasClass('sp-has-child')){
                //alert();
                $('.slicknav_nav .sp-menu-item.sp-has-child >a').after('<span class="fa icon"/>');
                
            }

            $('.slicknav_nav li.active').each(function() {
                $('.slicknav_nav .sp-mega-group-child li.active').parent().parent().addClass('active');
                $('.slicknav_nav li.active > span').addClass('expanded');
            });
            $('.slicknav_nav li.sp-menu-item.active > .sp-dropdown').css('display','block');
            $('.slicknav_nav li.sp-menu-item.active > .sp-dropdown > li.active > .sp-dropdown').css('display','block');
            $('.slicknav_nav li.sp-menu-item.active > .sp-dropdown > li.item-header.active > .sp-mega-group-child').css('display','block');  

            $('.slicknav_nav li.sp-menu-item .sp-dropdown , .slicknav_nav li.sp-menu-item .sp-mega-group-child').each(function(index) {
                $(this).prev().addClass('close').click(function() {
                    if ($(this).next().css('display') == 'none') {
                     $(this).next().slideDown(200, function () {
                        $(this).prev().removeClass('collapsed').addClass('expanded');
                        //$(this).prev().find('.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
                        $(this).parent().addClass('bgact');
                      });
                    }else {
                      $(this).next().slideUp(200, function () {
                        $(this).prev().removeClass('expanded').addClass('collapsed');
                        //$(this).prev().find('.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
                        $(this).parent().removeClass('bgact');
                        $(this).find('ul').each(function() {
                          $(this).hide().prev().removeClass('expanded').addClass('collapsed');
                          //$(this).hide().prev().find('.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
                        });
                      });
                    }
                return false;
                });
            });
        }     
    });
        var o=$('input[type=checkbox]');
        if(o.length&&!$('body').hasClass('.com_config')){
            o.each(function(count){
                if($(this).parent().not('span.checkbox')){
                    if(!$(this).attr("id")){
                        $(this).attr({id:'checkbox'+count}).wrap('<span class="checkbox"/>').after('<label class="checkbox_inner" for="checkbox'+count+'"/>')
                    }else{
                        $(this).wrap('<span class="checkbox"/>').after('<label class="checkbox_inner" for="'+$(this).attr("id")+'"/>')
                    }
                }
            })
        }
        var o=$('input[type=radio]');
        if(o.length&&!$('body').hasClass('.com_config')){
            o.each(function(i){
                if($(this).parent().not('span.radio')){
                    if(!$(this).attr("id")){
                        $(this).attr({id:'radio'+i}).wrap('<span class="radio"/>').after('<label class="radio_inner" for="radio'+i+'"/>')
                    }else{
                        $(this).wrap('<span class="radio"/>').after('<label class="radio_inner" for="'+$(this).attr("id")+'"/>')
                    }
                }
            })
        }
    });
})(jQuery);
    jQuery(document).ready(function(){
    jQuery('.sp-module.search .search-toogle').on('click touchmove',function(e) {
        jQuery('.sp-module.search').toggleClass('open');
        jQuery('body').toggleClass('modals-open');
    });
  
    jQuery(document).on('click touchmove',function(e) {
      var container = jQuery(".sp-module.search");
      if (!container.is(e.target)
          && container.has(e.target).length === 0 && container.hasClass('open'))
        {
          jQuery('.sp-module.search').toggleClass('open');
          jQuery('body').toggleClass('modals-open');
        }
    });

    jQuery('.sp-search .sropen').click(function(){
        jQuery('.sp-search').toggleClass('open');
        jQuery('body').toggleClass('modal-opens');
    });
    jQuery(document).on('click touchmove',function(e) {
      var container = jQuery(".sp-search");
      if (!container.is(e.target)
          && container.has(e.target).length === 0 && container.hasClass('open'))
        {
          jQuery('.sp-search').toggleClass('open');
          jQuery('body').toggleClass('modal-opens');
        }
    });
});
jQuery(document).ready(function() {
    var sl = jQuery('.forest-slider , .homepage-consulting , .showcase-banner');

    if(sl.length){
    var navheight = jQuery('#sp-header').outerHeight() + jQuery('#sp-top-bar').outerHeight();
        jQuery("#sp-header").css({ marginBottom : -navheight}).addClass('bg');
        jQuery("#sp-top-bar").addClass('bg');
    }
});
    
(function ($) {
    $(window).on("load", function(){
        var headerHeight = $("#sp-header").height();
        var topMenu = $(".sp-megamenu-wrapper"),
            topMenuHeight = headerHeight,
            // All list items
            menuItems = topMenu.find("li > a[href*='#']"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function(){
              var item = $($(this).attr("href"));
              if (item.length) {          
                return item; 
            }
            });
        // Bind to scroll
        $(window).scroll(function(){
           // Get container scroll position
           var fromTop = $(this).scrollTop()+topMenuHeight;
           // Get id of current scroll item
           var cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop){
                return this;
            }
            if($(document).height() - $(window).scrollTop()-1 < $(window).height()){               
                return this;
            }
           });
           // Get the id of the current element
           cur = cur[cur.length-1];
           var id = cur && cur.length ? cur[0].id : "";
           // Set/remove active class
           menuItems
             .parent().removeClass("active")
             .end().filter("[href='#"+id+"']").parent().addClass("active");
        });
        $( ".sp-megamenu-wrapper li > a[href*='#']" ).click(function( event ) {
            event.preventDefault();
            $('.sppb-section').removeClass('test');
            $("html, body").animate({ scrollTop: $($(this).attr("href")).addClass('test').offset().top }, 500);
            if($(window).width() < 768){
                $("body").toggleClass("default-offcanvas off-canvas-menu-init offcanvas")
            }
        });
    });
})(jQuery);

