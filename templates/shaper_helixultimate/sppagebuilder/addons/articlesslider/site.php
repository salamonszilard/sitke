<?php
/**
* @package SP Page Builder
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2016 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('resticted access');

class SppagebuilderAddonArticlesslider extends SppagebuilderAddons{

	public function render(){
		$class = (isset($this->addon->settings->class) && $this->addon->settings->class) ? $this->addon->settings->class : '';
		$style = (isset($this->addon->settings->style) && $this->addon->settings->style) ? $this->addon->settings->style : 'panel-default';
		$title = (isset($this->addon->settings->title) && $this->addon->settings->title) ? $this->addon->settings->title : '';
		$heading_selector = (isset($this->addon->settings->heading_selector) && $this->addon->settings->heading_selector) ? $this->addon->settings->heading_selector : 'h3';

		// Addon options
		$resource 		= (isset($this->addon->settings->resource) && $this->addon->settings->resource) ? $this->addon->settings->resource : 'article';
		$catid 			= (isset($this->addon->settings->catid) && $this->addon->settings->catid) ? $this->addon->settings->catid : 0;
		$tagids 		= (isset($this->addon->settings->tagids) && $this->addon->settings->tagids) ? $this->addon->settings->tagids : array();
		$k2catid 		= (isset($this->addon->settings->k2catid) && $this->addon->settings->k2catid) ? $this->addon->settings->k2catid : 0;
		$include_subcat = (isset($this->addon->settings->include_subcat)) ? $this->addon->settings->include_subcat : 1;
		$post_type 		= (isset($this->addon->settings->post_type) && $this->addon->settings->post_type) ? $this->addon->settings->post_type : '';
		$ordering 		= (isset($this->addon->settings->ordering) && $this->addon->settings->ordering) ? $this->addon->settings->ordering : 'latest';
		$limit 			= (isset($this->addon->settings->limit) && $this->addon->settings->limit) ? $this->addon->settings->limit : 3;
		$columns 		= (isset($this->addon->settings->columns) && $this->addon->settings->columns) ? $this->addon->settings->columns : 3;
		$show_intro 	= (isset($this->addon->settings->show_intro)) ? $this->addon->settings->show_intro : 1;
		$intro_limit 	= (isset($this->addon->settings->intro_limit) && $this->addon->settings->intro_limit) ? $this->addon->settings->intro_limit : 200;
		$hide_thumbnail = (isset($this->addon->settings->hide_thumbnail)) ? $this->addon->settings->hide_thumbnail : 0;
		$show_author 	= (isset($this->addon->settings->show_author)) ? $this->addon->settings->show_author : 1;
		$show_category 	= (isset($this->addon->settings->show_category)) ? $this->addon->settings->show_category : 1;
		$show_date 		= (isset($this->addon->settings->show_date)) ? $this->addon->settings->show_date : 1;
		$show_readmore 	= (isset($this->addon->settings->show_readmore)) ? $this->addon->settings->show_readmore : 1;
		$readmore_text 	= (isset($this->addon->settings->readmore_text) && $this->addon->settings->readmore_text) ? $this->addon->settings->readmore_text : 'Read More';
		$link_articles 	= (isset($this->addon->settings->link_articles)) ? $this->addon->settings->link_articles : 0;
		$link_catid 	= (isset($this->addon->settings->link_catid)) ? $this->addon->settings->link_catid : 0;
		$link_k2catid 	= (isset($this->addon->settings->link_k2catid)) ? $this->addon->settings->link_k2catid : 0;
		


		$output   = '';
		//include k2 helper
		$k2helper 			= JPATH_ROOT . '/components/com_sppagebuilder/helpers/k2.php';
		$article_helper = JPATH_ROOT . '/components/com_sppagebuilder/helpers/articles.php';
		$isk2installed  = self::isComponentInstalled('com_k2');
		$document 	= JFactory::getDocument();
		$app =& JFactory::getApplication();
		$template = $app->getTemplate();

		$document->addStyleSheet(JURI::base() .'templates/'.$template.'/sppagebuilder/addons/articlesslider/css/slick.css');
		$slicpath = JURI::base() .'templates/'.$template.'/sppagebuilder/addons/articles/js/slick.min.js';
		$document->addScript($slicpath);

		if ($resource == 'k2') {
			if ($isk2installed == 0) {
				$output .= '<p class="alert alert-danger">' . JText::_('COM_SPPAGEBUILDER_ADDON_ARTICLE_ERORR_K2_NOTINSTALLED') . '</p>';
				return $output;
			} elseif(!file_exists($k2helper)) {
				$output .= '<p class="alert alert-danger">' . JText::_('COM_SPPAGEBUILDER_ADDON_K2_HELPER_FILE_MISSING') . '</p>';
				return $output;
			} else {
				require_once $k2helper;
			}
			$items = SppagebuilderHelperK2::getItems($limit, $ordering, $k2catid, $include_subcat);
		} else {
			require_once $article_helper;
			$items = SppagebuilderHelperArticles::getArticles($limit, $ordering, $catid, $include_subcat, $post_type, $tagids);
		}

		if (!count($items)) {
			$output .= '<p class="alert alert-warning">' . JText::_('COM_SPPAGEBUILDER_ADDON_ARTICLE_NO_ITEMS_FOUND') . '</p>';
			return $output;
		}

		if(count((array) $items)) {
			$output  .= '<div class="sppb-addon sppb-addon-articlesslider ' . $class . '">';

			if($title) {
				$output .= '<'.$heading_selector.' class="sppb-addon-title">' . $title . '</'.$heading_selector.'>';
			}

			$output .= '<div class="sppb-addon-content">';
			$output	.= '<div class="sppb-row">';
			$output .= '<div class="sppb-col-sm-9 slider-for">';

			foreach ($items as $key => $item) {
				$output .= '<div class="sppb-addon-article">';
   	          	require_once(JPATH_SITE.'/components/com_k2/models/item.php');
  				$model = K2Model::getInstance('Item', 'K2Model');

				if(!$hide_thumbnail) {
					$image = '';
					if ($resource == 'k2') {
						$image = $item->image_large;
					} 
					$output .= '<div class="image-box">';  


			              $tags = $model->getItemTags($item->id);
  			           if($tags) {
			            // print_r($tags);
			              $output .= '<ul class="sppb-tag">';
			              foreach ($tags as $tag){ 
			                if($tag->name != ''){           
			                 $output .= '<li><span class="list-tag">'.$tag->name.'</span></li>';
			                }
			              }
			              $output .= '</ul>';
			             // 
			            }  
					if(isset($image) && $image) {
						$output .= '<img class="sppb-img-responsive" src="'. $image .'" alt="'. $item->title .'" itemprop="thumbnailUrl">';
					}
					$output .= '</div>'; 
				}
				$output .= '<div class="blog-box">';
			        $item->numOfComments = $model->countItemComments($item->id);
			        if($show_author || $show_category || $show_date || $show_tags) {
			          $output .= '<div class="sppb-article-meta">';

			          if($show_category) {

			            if ($resource == 'k2') {
			              $item->catUrl = urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($item->catid.':'.urlencode($item->category_alias))));
			            } else {
			              $item->catUrl = JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug));
			            }

			            $output .= '<span class="sppb-meta-category"><i class="linearicons-folder"></i>' . $item->category . '</span>';
			          }
			          if($show_date) {
			            $output .= '<span class="sppb-meta-date" itemprop="dateCreated"><i class="linearicons-calendar-full"></i>' . Jhtml::_('date', $item->created, 'DATE_FORMAT_LC3') . '</span>';
			          }
			          if($show_author) {
			              if($item->created_by_alias) {
			                $output .= '<span class="sppb-meta-author" itemprop="name"><i class="linearicons-user"></i>' . $item->created_by_alias . '</span>';
			              }
			          }
			           $output .= '<span class="komento">';
			          $output .= '<i class="linearicons-bubble"></i>'.$item->numOfComments;
			          $output .= '</span>';
			          $output .= '<span class="hits">';
			          $output .= '<i class="linearicons-heart"></i>'.$item->hits;
			          $output .= '</span>';

			          $output .= '</div>';
			        }
			        $output .= '<div class="name-blog"><a  href="'. $item->link .'" itemprop="url">' . $item->title . '</a></div>';
			        $output .= $item->beforeDisplayContent;
			        $output .= $item->afterDisplayTitle;

			        if($show_intro) {
			          $item->introtext = strip_tags($item->introtext);
			          $output .= '<div class="sppb-article-introtext">'. Jhtml::_('string.truncate', ($item->introtext), $intro_limit) .'</div>';
			        }
			        $output .= $item->afterDisplayContent;
			        if($show_readmore || $show_date) {
			           $output .= '<div class="btn-pos">';
			          if($show_readmore) {
			            $output .= '<a class="sppb-readmore sppb-btn sppb-btn-link" href="'. $item->link .'" itemprop="url"><i class="linearicons-chevron-right-circle"></i>'. $readmore_text .' </a>';
			          }
			          $output .= '</div>';
			       }
        		$output .= '</div>';
				
				$output .= '</div>';
				
			}
			$output .= '</div>';
			$output .= '<div class="sppb-col-sm-3 slider-nav">';
			foreach ($items as $key => $item) {
				$output .= '<div class="sppb-addon-article">';

				if(!$hide_thumbnail) {
					$image = '';
					if ($resource == 'k2') {
						$image = $item->image_medium;
					} 
					
					if(isset($image) && $image) {
						$output .= '<img class="sppb-img-responsive" src="'. $image .'" alt="'. $item->title .'" itemprop="thumbnailUrl">';
					}
				}
				$item->title = strip_tags($item->title);
				$output .= '<div class="name-blog">'. Jhtml::_('string.truncate', ($item->title), 55) .'</div>';
				$item->numOfComments = $model->countItemComments($item->id);
				if($show_author || $show_category || $show_date || $show_tags) {
			          $output .= '<div class="clear"></div><div class="sppb-article-meta">';

			          if($show_category) {

			            if ($resource == 'k2') {
			              $item->catUrl = urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($item->catid.':'.urlencode($item->category_alias))));
			            } else {
			              $item->catUrl = JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug));
			            }

			            $output .= '<span class="sppb-meta-category"><i class="linearicons-folder"></i>' . $item->category . '</span>';
			          }
			          if($show_date) {
			            $output .= '<span class="sppb-meta-date" itemprop="dateCreated"><i class="linearicons-calendar-full"></i>' . Jhtml::_('date', $item->created, 'DATE_FORMAT_LC3') . '</span>';
			          }
			          if($show_author) {
			              if($item->created_by_alias) {
			                $output .= '<span class="sppb-meta-author" itemprop="name"><i class="linearicons-user"></i>' . $item->created_by_alias . '</span>';
			              }
			          }
			           //$output .= '<span class="komento">';
			          //$output .= '<i class="linearicons-bubble"></i>'.$item->numOfComments;
			          //$output .= '</span>';
			          $output .= '<span class="hits">';
			          $output .= '<i class="linearicons-heart"></i>'.$item->hits;
			          $output .= '</span>';

			          $output .= '</div>';
			        }
				$output .= '</div>';
				
			}
			$output .= '</div>';

			$output  .= '</div>';
			$output  .= '</div>';
			$output  .= '</div>';
		}

		return $output;
	}

	public function css() {
		$addon_id = '#sppb-addon-' .$this->addon->id;
		$layout_path = JPATH_ROOT . '/components/com_sppagebuilder/layouts';
		$css_path = new JLayoutFile('addon.css.button', $layout_path);

		$options = new stdClass;
		$options->button_type = (isset($this->addon->settings->all_articles_btn_type) && $this->addon->settings->all_articles_btn_type) ? $this->addon->settings->all_articles_btn_type : '';
		$options->button_appearance = (isset($this->addon->settings->all_articles_btn_appearance) && $this->addon->settings->all_articles_btn_appearance) ? $this->addon->settings->all_articles_btn_appearance : '';
		$options->button_color = (isset($this->addon->settings->all_articles_btn_color) && $this->addon->settings->all_articles_btn_color) ? $this->addon->settings->all_articles_btn_color : '';
		$options->button_color_hover = (isset($this->addon->settings->all_articles_btn_color_hover) && $this->addon->settings->all_articles_btn_color_hover) ? $this->addon->settings->all_articles_btn_color_hover : '';
		$options->button_background_color = (isset($this->addon->settings->all_articles_btn_background_color) && $this->addon->settings->all_articles_btn_background_color) ? $this->addon->settings->all_articles_btn_background_color : '';
		$options->button_background_color_hover = (isset($this->addon->settings->all_articles_btn_background_color_hover) && $this->addon->settings->all_articles_btn_background_color_hover) ? $this->addon->settings->all_articles_btn_background_color_hover : '';
		$options->button_fontstyle = (isset($this->addon->settings->all_articles_btn_fontstyle) && $this->addon->settings->all_articles_btn_fontstyle) ? $this->addon->settings->all_articles_btn_fontstyle : '';
		$options->button_letterspace = (isset($this->addon->settings->all_articles_btn_letterspace) && $this->addon->settings->all_articles_btn_letterspace) ? $this->addon->settings->all_articles_btn_letterspace : '';

		return $css_path->render(array('addon_id' => $addon_id, 'options' => $options, 'id' => 'btn-' . $this->addon->id));
	}
	public function js() {
		$show_carousel 		= (isset($this->addon->settings->show_carousel)) ? $this->addon->settings->show_carousel : 1;
		$loop 		= (isset($this->addon->settings->loop)) ? $this->addon->settings->loop : 0;
		$nav 		= (isset($this->addon->settings->nav)) ? $this->addon->settings->nav : 0;
		$autoplay 		= (isset($this->addon->settings->autoplay)) ? $this->addon->settings->autoplay: 0;
		$autoplayTimeout	= (isset($this->addon->settings->autoplayTimeout)) ? $this->addon->settings->autoplayTimeout : 1200;
		$rewind 		= (isset($this->addon->settings->rewind)) ? $this->addon->settings->rewind : 0;
		$responsive_computer 		= (isset($this->addon->settings->responsive_computer)) ? $this->addon->settings->responsive_computer : 3;
		$responsive_laptop 		= (isset($this->addon->settings->responsive_laptop)) ? $this->addon->settings->responsive_laptop : 3;
		$responsive_tablet 		= (isset($this->addon->settings->responsive_tablet)) ? $this->addon->settings->responsive_tablet : 2;
		$responsive_mobile 		= (isset($this->addon->settings->responsive_computer)) ? $this->addon->settings->responsive_mobile : 1;
		$jcorusel = '
      	jQuery(document).ready(function($) {
			$("#sppb-addon-'.$this->addon->id.' .sppb-addon-articlesslider .slider-for").slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: true,
			  fade: true,
			  //autoplay: true,
 			 //autoplaySpeed: 12000,
			  asNavFor: ".slider-nav",
			   infinite: true
			});
			$(".slider-nav").slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  asNavFor: ".slider-for",
			  dots: false,
			  arrows: false,
			  centerMode: false,
			  vertical:true,
			  focusOnSelect: true,
			  infinite: false
			});
		});
		';		 
      return $jcorusel;
    }
	static function isComponentInstalled($component_name){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select( 'a.enabled' );
		$query->from($db->quoteName('#__extensions', 'a'));
		$query->where($db->quoteName('a.name')." = ".$db->quote($component_name));
		$db->setQuery($query);
		$is_enabled = $db->loadResult();
		return $is_enabled;
	}

}

